package com.example.springbatch.web;

import com.example.springbatch.batch.io.InMemoryQueue;
import com.example.springbatch.data.entity.Person;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Entity;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.NoSuchJobExecutionException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("jobs")
public class Controller {

    private final JobLauncher launcher;
    private final Job job;
    private final JobExplorer explorer;
    private final JobOperator operator;
    private final InMemoryQueue queue;

    public Controller(final @Qualifier("launcher") JobLauncher launcher,
                      final @Qualifier("default") Job job,
                      final JobExplorer explorer,
                      final JobOperator operator,
                      final ThreadPoolTaskExecutor executor, InMemoryQueue queue) {
        this.launcher = launcher;
        this.job = job;
        this.explorer = explorer;
        this.operator = operator;
        this.queue = queue;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Long> start(@RequestBody FileNameContainer fileNameContainer) throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobInstanceAlreadyCompleteException {
        JobParameters jobParameters =
                new JobParametersBuilder()
                        .addLong("id", System.nanoTime())
                        .addString("fileName", fileNameContainer.getFileName())
                        .toJobParameters();

        try {
            final JobExecution run = launcher.run(job, jobParameters);
            return ResponseEntity.ok(run.getJobInstance().getInstanceId());
        } catch (JobRestartException e) {
            return ResponseEntity.status(409).build();
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<String> getAll() {

        final List<JobInstance> jobInstances = explorer.getJobInstances("importUserJob", 0, Integer.MAX_VALUE);

        final StringBuilder sb = new StringBuilder();
        for (JobInstance instance : jobInstances) {
            sb.append("Job instance: ")
                    .append(instance.getId())
                    .append("\n");

            explorer.getJobExecutions(instance).forEach(execution -> {
                sb.append("\t")
                        .append("status :")
                        .append(execution.getStatus())
                        .append("\n");
            });
            sb.append("\n");
        }

        return ResponseEntity.ok(sb.toString());
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> getStatus(@PathVariable("id") Long id) {

        final JobInstance jobInstance1 = explorer.getJobInstance(id);
        final List<JobExecution> jobExecutions = explorer.getJobExecutions(jobInstance1);


        if (jobExecutions.isEmpty()) {
            return ResponseEntity.status(200).body("This job is currently not being executed.");
        }

        final String result = jobExecutions.stream().map(this::getExecutionDescription).collect(Collectors.joining("\n"));

        return ResponseEntity.ok(result);
    }

    private String getExecutionDescription(JobExecution execution){
        final StringBuilder sb = new StringBuilder();
        final BatchStatus exitStatus = execution.getStatus();
        sb.append("\texecution: ").append(execution.getId()).append("\tstatus: ").append(exitStatus);

        if (BatchStatus.STOPPED.equals(exitStatus)) {
            final List<Person> skippedPersons = (List<Person>) (execution.getExecutionContext().get("pp"));
            if (skippedPersons != null && !skippedPersons.isEmpty()) {
                sb.append("\n")
                        .append("\tinvalid names are: ")
                        .append(skippedPersons);
            }
        }

        sb.append("\n");

        return sb.toString();
    }



    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> cancel(@PathVariable("id") Long id) throws NoSuchJobExecutionException, JobExecutionAlreadyRunningException {
        final JobInstance jobInstance = explorer.getJobInstance(id);
        queue.removeQueue(jobInstance.getId());

        final List<Long> executionIds = explorer.getJobExecutions(jobInstance).stream().map(Entity::getId).collect(Collectors.toList());

        for (long jobExecutionId : executionIds) {
            operator.abandon(jobExecutionId);
        }

        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<String> resume(@PathVariable("id") Long id) throws JobParametersInvalidException, JobRestartException, JobInstanceAlreadyCompleteException, JobExecutionAlreadyRunningException {
        final JobInstance jobInstance = explorer.getJobInstance(id);

        final List<JobExecution> jobExecutions = explorer.getJobExecutions(jobInstance);
        final JobParameters jobParameters = jobExecutions.get(0).getJobParameters();

        System.out.println("continuing execution of job " + jobInstance);
        launcher.run(job, jobParameters);

        return ResponseEntity.ok().build();
    }


}
