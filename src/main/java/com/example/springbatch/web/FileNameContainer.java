package com.example.springbatch.web;

public class FileNameContainer {
    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public FileNameContainer setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }
}
