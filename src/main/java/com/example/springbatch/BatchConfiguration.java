package com.example.springbatch;

import com.example.springbatch.batch.JobCompletionNotificationListener;
import com.example.springbatch.batch.PersonWithMetadata;
import com.example.springbatch.batch.QueueManagementListener;
import com.example.springbatch.batch.ValidationListener;
import com.example.springbatch.batch.io.DbWriter;
import com.example.springbatch.batch.io.QueueReader;
import com.example.springbatch.batch.io.QueueWriter;
import com.example.springbatch.batch.processor.UppercaseProcessor;
import com.example.springbatch.batch.processor.ValidatingProcessor;
import com.example.springbatch.data.entity.Person;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.configuration.support.JobRegistryBeanPostProcessor;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.listener.ExecutionContextPromotionListener;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    private static final String WILL_BE_INJECTED = null;

    @Autowired
    public JobRepository jobRepository;

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    public QueueWriter queueWriter;

    @Autowired
    public QueueReader queueReader;

    @Autowired
    public DbWriter dbWriter;

    @Autowired
    public ValidatingProcessor validatingProcessor;

    @Autowired
    public UppercaseProcessor uppercaseProcessor;

    @Autowired
    public ValidationListener validationListener;

    @Autowired
    public QueueManagementListener queueManagementListener;

    @Autowired
    DataSource dataSource;


    @Bean
    public JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor(JobRegistry jobRegistry) {
        JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor = new JobRegistryBeanPostProcessor();
        jobRegistryBeanPostProcessor.setJobRegistry(jobRegistry);
        return jobRegistryBeanPostProcessor;
    }


    @Bean
    @StepScope
    public FlatFileItemReader<Person> fileReader(@Value("#{jobParameters['fileName']}")String fileName) {
        return new FlatFileItemReaderBuilder<Person>()
                .name("personItemReader")
                .resource(new ClassPathResource(fileName))
                .delimited()
                .names("firstName", "lastName")
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Person>() {{
                    setTargetType(Person.class);
                }})
                .build();
    }


    @Bean(name = "default")
    public Job importUserJob(JobCompletionNotificationListener listener,
                             QueueManagementListener queueManagementListener)
                     {
        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .listener(queueManagementListener)
                .start(step1())
                .next(step2())
                .build();
    }

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .<Person, PersonWithMetadata>chunk(2)
                .reader(fileReader(WILL_BE_INJECTED))
                .processor(validatingProcessor)
                .writer(queueWriter)
                .listener(promotionListener())
                .listener(validationListener)
                .build();
    }

    @Bean
    public Step step2() {
        return stepBuilderFactory.get("step2")
                .<PersonWithMetadata, Person>chunk(2)
                .reader(queueReader)
                .processor(uppercaseProcessor)
                .writer(dbWriter)
                .build();
    }



    @Bean(name = "launcher")
    public JobLauncher jobLauncher() {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(jobRepository);
        jobLauncher.setTaskExecutor(new SimpleAsyncTaskExecutor());
        return jobLauncher;
    }

    @Bean
    public ExecutionContextPromotionListener promotionListener() {
        ExecutionContextPromotionListener listener = new ExecutionContextPromotionListener();

        listener.setKeys(new String[] {"pp"});

        return listener;
    }
}
