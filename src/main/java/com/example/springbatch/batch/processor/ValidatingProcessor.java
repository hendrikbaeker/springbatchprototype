package com.example.springbatch.batch.processor;

import com.example.springbatch.batch.PersonWithMetadata;
import com.example.springbatch.data.entity.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Service;

@Service
public class ValidatingProcessor implements ItemProcessor<Person, PersonWithMetadata> {

    private static final Logger log = LoggerFactory.getLogger(ValidatingProcessor.class);

    @Override
    public PersonWithMetadata process(final Person person) {

        log.info("Validating (" + person + ")");
        final int lengthOfFirstName = person.getFirstName().length();
        return new PersonWithMetadata(person, lengthOfFirstName<=3);
    }
}
