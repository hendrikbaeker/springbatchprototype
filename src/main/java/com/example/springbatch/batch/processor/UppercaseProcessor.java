package com.example.springbatch.batch.processor;

import com.example.springbatch.batch.PersonWithMetadata;
import com.example.springbatch.data.entity.Person;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Service;

@Service
public class UppercaseProcessor implements ItemProcessor<PersonWithMetadata, Person> {
    @Override
    public Person process(PersonWithMetadata item) {

        final String firstName = item.getPerson().getFirstName().toUpperCase();
        final String lastName = item.getPerson().getLastName().toUpperCase();

        return new Person(firstName, lastName);
    }
}
