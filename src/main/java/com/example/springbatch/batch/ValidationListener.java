package com.example.springbatch.batch;

import com.example.springbatch.data.entity.Person;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class ValidationListener implements StepExecutionListener, Ordered {
    @Override
    public void beforeStep(StepExecution stepExecution) {

    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        ExecutionContext stepContext = stepExecution.getExecutionContext();

        List<Person> pp = (List<Person>) (stepContext.get("pp"));
        if(pp!=null && !pp.isEmpty()){
            stepExecution.getJobExecution().stop();

        }
        return ExitStatus.COMPLETED;
    }

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }
}
