package com.example.springbatch.batch;

import com.example.springbatch.data.entity.Person;

public class PersonWithMetadata {
    private Person person;
    private boolean isTooShort;

    public PersonWithMetadata(Person person, boolean isTooShort) {
        this.person = person;
        this.isTooShort = isTooShort;
    }

    public Person getPerson() {
        return person;
    }

    public boolean isTooShort() {
        return isTooShort;
    }
}
