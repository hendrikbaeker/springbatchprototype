package com.example.springbatch.batch;

import com.example.springbatch.batch.io.InMemoryQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.stereotype.Component;

@Component
public class QueueManagementListener extends JobExecutionListenerSupport {

    private static final Logger log = LoggerFactory.getLogger(QueueManagementListener.class);

    private final InMemoryQueue queues;

    public QueueManagementListener(InMemoryQueue queues) {
        this.queues = queues;
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        final BatchStatus batchStatus = jobExecution.getStatus();
        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            queues.removeQueue(jobExecution.getJobInstance().getInstanceId());
        }
    }

    @Override
    public void beforeJob(JobExecution jobExecution) {
        queues.setUpQueue(jobExecution.getJobInstance().getInstanceId());
    }
}
