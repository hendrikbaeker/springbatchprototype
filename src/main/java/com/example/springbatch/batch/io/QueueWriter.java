package com.example.springbatch.batch.io;

import com.example.springbatch.batch.PersonWithMetadata;
import com.example.springbatch.data.entity.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class QueueWriter implements ItemWriter<PersonWithMetadata> {

    private final InMemoryQueue queue;
    private StepExecution stepExecution;
    private final Logger log = LoggerFactory.getLogger(QueueWriter.class);

    public QueueWriter(final InMemoryQueue queue) {
        this.queue = queue;
    }


    @Override
    public void write(List<? extends PersonWithMetadata> items) throws Exception {
        ExecutionContext executionContext = stepExecution.getExecutionContext();
        for (PersonWithMetadata personWithMetadata : items) {

            if (personWithMetadata.isTooShort()) {



                List<Person> pp = (List<Person>) (executionContext.get("pp"));
                final List<Person> nn = pp == null ? new ArrayList<Person>() : pp;

                nn.add(personWithMetadata.getPerson());
                executionContext.put("pp", nn);
                log.info("Skipping person because name is too short. name={}", personWithMetadata.getPerson().getFirstName());
            } else {


                if (personWithMetadata != null) {
                    queue.add(stepExecution.getJobExecution().getJobInstance().getInstanceId(), personWithMetadata);
                }
            }
        }
    }

    @BeforeStep
    public void saveStepExecution(StepExecution stepExecution) {
        this.stepExecution = stepExecution;
    }
}
