package com.example.springbatch.batch.io;

import com.example.springbatch.batch.PersonWithMetadata;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

@Service
public class InMemoryQueue {

private Map<Long, Queue<PersonWithMetadata>>    queues;

    public InMemoryQueue() {
        queues = new HashMap<>();
    }

    public void add(Long executionId, PersonWithMetadata person){
        final Queue<PersonWithMetadata> queueForExecutionId = queues.get(executionId);
        queueForExecutionId.add(person);
    }

    public PersonWithMetadata poll(Long executionId){
        return queues.get(executionId).poll();
    }


    public void setUpQueue(Long id) {
        queues.putIfAbsent(id, new LinkedList<>());
    }

    public void removeQueue(Long id) {
        queues.remove(id);
    }
}
