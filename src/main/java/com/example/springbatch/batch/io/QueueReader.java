package com.example.springbatch.batch.io;

import com.example.springbatch.batch.PersonWithMetadata;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemReader;
import org.springframework.stereotype.Service;

@Service
public class QueueReader implements ItemReader<PersonWithMetadata> {

    private final InMemoryQueue queue;
    private StepExecution stepExecution;


    public QueueReader(final InMemoryQueue queue) {
        this.queue = queue;
    }

    @Override
    public PersonWithMetadata read() {
        final Long jobExecutionId = stepExecution.getJobExecution().getJobInstance().getInstanceId();
        return queue.poll(jobExecutionId);
    }

    @BeforeStep
    public void saveStepExecution(StepExecution stepExecution) {
        this.stepExecution = stepExecution;
    }
}
