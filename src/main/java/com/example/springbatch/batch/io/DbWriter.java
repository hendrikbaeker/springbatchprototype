package com.example.springbatch.batch.io;

import com.example.springbatch.data.entity.Person;
import com.example.springbatch.data.repository.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DbWriter implements ItemWriter<Person> {

    private static final Logger log = LoggerFactory.getLogger(DbWriter.class);

    private final PersonRepository repository;

    public DbWriter(final PersonRepository repository) {
        this.repository = repository;
    }


    @Override
    @Transactional
    public void write(List<? extends Person> items) {

        for (Person person : items) {
            repository.save(person);
            log.info("Persisting person. name={}", person.getFirstName());
        }
    }



}
